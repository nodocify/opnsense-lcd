All commands are _Escaped_ with hex 0x1b, and are quiet comprehensive... except for very basic command

according to the CP's drivers each command bloc was followed by an `usleep(1000);` function...

| Commands | ASCII Cmd | write size | Test in G500 |	Find In Tap	| Descr | 
| ------ | ----- | ----- | ------ | ------ | ------ |
|0x0C | n/a | 1 | YES | | clear Screen |
|0x0b | n/a | 1 | YES | | home Cursor |
|0x1B 0x40 | `ESC` @ | 2 |	YES	|  |	Display Init |
|0x1B 0x42 0n | `ESC` B _n_ | 3 |YES |	YES | Back Light State (0..???) ? |
|0x1B 0x47 [1024 bytes]| `ESC` G ... | 1026 | YES | YES | Upload Picture (1) |
|0x1B 0x12 | `ESC` n/a | 2 |  |  | 	Vertical scroll mode |
|0x1B 0x13 | `ESC` n/a | 2 |  |  | Horizontal Scroll mode |
|0x1B 0x4C n | `ESC`L _n_ | 3 | YES | | LED Control (2) |
|0x1B 0x5B 0x41 | `ESC` [ A | 3 | YES |  | Move Cursor Up|
|0x1B 0x5B 0x42 | `ESC` [ B | 3 | YES |  | Move Cursor Down|
|0x1B 0x5B 0x43 | `ESC` [ C | 3 |	YES | |	Move Cursor Right|
|0x1B 0x5B 0x44 | `ESC` [ D | 3 |	YES	| | Move Cursor Left|
|0x1B 0x5B 0x48 | `ESC` [ H | 3 |	 | | Move Cursor Home|
|0x1B 0x5B 0x4B | `ESC` [ K | 3 |	| | Move Cursor Bottom position ?|
|0x1B 0x5B 0x4C | `ESC` [ L | 3 | | | Move Cursor Left Most position ?|
|0x1B 0x5B 0x52 | `ESC` [ R | 3 | | | Move Cursor Right Most position ?|
|0x1B 0x5F 0x0n | `ESC` _ _n_ | 3 |	 | | Hide Cursor on/off (1/0)|
|0x1B 0x72 0x0n | `ESC` r *n* | 3 |	YES	|  | Inverted character [n 0:OFF 1:ON]|
|0x1B 0x50 nn | `ESC` P _n_ | 3 |	YES	|  | Show Graphics Page nn |
|0x1B 0x53 nn | `ESC` S _n_ | 3 | | | Save Current Screen to Page nn |

**Notes**  


1. picture are 128x64 pixels, encoded 1 pixel per bits  
    A byte is displayed not as a 8 pixels Horizontal line (like on a conventionnal bitmap field), but as a 8 pixel vertical column...   
    This can be usefull to draw text, but painfull for graphics encoding.  
    This means that the byte SCREEN[0] display pixels 0,0 to 0,8 and the byte SCREEN[1] refert to pixels 1,0 to 1,8... Low Bit is row 0, high bit is row 7.
    When drawing to the screen, the picture is written in 2 64x64 halves, left then right.

2. All led were "bicolor" and were addressed as 2 pins out from the controler.  
