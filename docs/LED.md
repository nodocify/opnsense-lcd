All LEDs are in fact 2 LED (one red and one green) and seems to be connected to separated pins from the controller, or at least can be addressed by the controller following this addresses design:

```
LED 1 (r) = 1  
LED 1 (g) = 2
```
```
LED 2 (r) = 3  
LED 2 (g) = 4
```
```
LED 3 (r) = 5  
LED 3 (g) = 6
```

There is no zero LED    

the commands to address the LED status is 0x1B 0x4C _STATUS_  

_STATUS_ byte is divided in two parts:  
 4 high bits are used to define the LED id,  
 1 low bit is used to define the status (on/off)  

So Led's can controlled with the following (pseudo-c) code:
```
#define     LED_STATUS_ON               0x01
#define     LED_STATUS_OFF              0x00

#define     LED_1_RED                   0x10
#define     LED_1_GREEN                 0x20
#define     LED_2_RED                   0x30
#define     LED_2_GREEN                 0x40
#define     LED_3_RED                   0x50
#define     LED_3_GREEN                 0x60

void SetLed(unsigned char ledid,unsigned char status)
{
    unsigned char cmd[3];

    cmd[0] = "\x1b";
    cmd[1] = "\x4c";
    cmd[2] = (unsigned char) (ledid + status);

    SerialPtr_writebuffer(cmd,3);
}


SetLed(LED_1_GREEN, LED_STATUS_ON);
```

Notes:  

In the CP's drivers, the LED were addressed in a funny ways:

```
int _SetLedState(int led,int state)
{
  char led_id;
  int ret;
  char cmdRed [3];
  char cmdGreen [3];
  
  ret = -1;
  if (led - 1U < 3) 
  {
    led_id = (char)led * '\x02';
    cmdRed[2] = (led_id + -1) * '\x10' | (byte)state & 1;
    cmdGreen[2] = (led_id + -2) * '\x10' | (byte)((uint)state >> 1) & 1;
    cmdGreen[0] = '\x1b';
    cmdGreen[1] = 'L';
    cmdRed[0] = '\x1b';
    cmdRed[1] = 'L';
    SendCommand(fd,cmdGreen,3);
    SendCommand(fd,cmdRed,3);
    ret = 0;
  }
  return ret;
}
```
