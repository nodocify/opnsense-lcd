package main

import (
	"image/color"
	"image/png"
	"log"
	"os"
	"strconv"
	"time"

	"go.bug.st/serial"
)

func main() {
	mode := &serial.Mode{
		BaudRate: 115200,
	}
	port, err := serial.Open("COM6", mode)
	if err != nil {
		log.Fatal(err)
	}
	defer port.Close()
	init_lcd(port)
	time.Sleep(time.Second)
	draw_logo("opnsense-logo.png", port)
}

type bitString string

func (b bitString) AsByteSlice() []byte {
	var out []byte
	var str string

	for i := len(b); i > 0; i -= 8 {
		if i-8 < 0 {
			str = string(b[0:i])
		} else {
			str = string(b[i-8 : i])
		}
		v, err := strconv.ParseUint(reverse(str), 2, 8)
		if err != nil {
			panic(err)
		}
		out = append([]byte{byte(v)}, out...)
	}
	return out
}

func init_lcd(port serial.Port) {
	_, err := port.Write([]byte("\x1B\x40"))
	if err != nil {
		log.Fatal(err)
	}
}

func draw_logo(path string, port serial.Port) {
	logo := convertLogo(path)
	var x []byte
	y := bitString(logo).AsByteSlice()
	x = append([]byte("\x1B\x47"), y...)
	_, err := port.Write(x)
	if err != nil {
		log.Fatal(err)
	}
}

func reverse(s string) string {
	n := len(s)
	runes := make([]rune, n)
	for _, rune := range s {
		n--
		runes[n] = rune
	}
	return string(runes[n:])
}

func convertLogo(path string) string {
	imgFile, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer imgFile.Close()

	img, err := png.Decode(imgFile)
	if err != nil {
		log.Fatal(err)
	}

	if img.Bounds().Size().X != 128 && img.Bounds().Size().Y != 64 {
		log.Fatal("Image size not compatible. Must be 128x64.")
	}

	var output string
	startY := 0
	startX := 0

	for col := 0; col < 2; col++ {
		for row := 0; row < 8; row++ {
			for x := startX; x < startX+64; x++ {
				for y := startY; y < startY+8; y++ {
					c := color.GrayModel.Convert(img.At(x, y)).(color.Gray)
					switch c.Y {
					case 0:
						output += "0"
					default:
						output += "1"
					}
				}
			}
			startY += 8
		}
		startX += 64
		startY = 0
	}
	return output
}
